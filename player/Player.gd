extends KinematicBody2D

signal life_changed
signal dead

export (int) var run_speed
export (int) var jump_speed
export (int) var gravity
export (int) var climb_speed

enum State {IDLE, RUN, JUMP, HURT, DEAD, CROUCH, CLIMB}
var state
var anim
var new_anim
var velocity = Vector2()
var life
var max_jumps = 2
var jump_count = 0
var is_on_ladder = false

func _ready():
	change_state(State.IDLE)

func start(pos):
	position = pos
	show()
	change_state(State.IDLE)
	life = 3
	emit_signal("life_changed", life)

func change_state(new_state):
	state = new_state
	match state:
		State.IDLE:
			new_anim = "idle"
		State.RUN:
			new_anim = "run"
		State.HURT:
			new_anim = "hurt"
			velocity.y = -200
			velocity.x = -100 * sign(velocity.x)
			life -= 1
			emit_signal("life_changed", life)
			yield(get_tree().create_timer(0.5), "timeout")
			change_state(State.IDLE)
			if life <= 0:
				change_state(State.DEAD)
		State.JUMP:
			new_anim = "jump_up"
			jump_count = 1
		State.CROUCH:
			new_anim = "crouch"
		State.CLIMB:
			new_anim = "climb"
		State.DEAD:
			emit_signal("dead")
			hide()

func get_input():
	if state == State.HURT:
		return # don't allow movement during hurt state
	var right = Input.is_action_pressed("right")
	var left = Input.is_action_pressed("left")
	var jump = Input.is_action_just_pressed("jump")
	var down = Input.is_action_pressed("crouch")
	var climb = Input.is_action_pressed("climb")
	
	# movement occurs in all states
	velocity.x = 0
	if right:
		velocity.x += run_speed
		$Sprite.flip_h = false
	if left:
		velocity.x -= run_speed
		$Sprite.flip_h = true
	if jump:
		if (state == State.JUMP) and (jump_count < max_jumps):
			new_anim = "jump_up"
			velocity.y = jump_speed / 1.5
			jump_count += 1
		if is_on_floor():
			$JumpSound.play()
			change_state(State.JUMP)
			velocity.y = jump_speed
	if down and is_on_floor():
		change_state(State.CROUCH)
	if climb and (state != State.CLIMB) and is_on_ladder:
		change_state(State.CLIMB)
	# IDLE transitions to RUN when moving
	if (state in [State.IDLE, State.CROUCH]) and (velocity.x !=0):
		change_state(State.RUN)
	# RUN transitions to IDLE when standing still
	if (state == State.RUN) and (velocity.x == 0):
		change_state(State.IDLE)
	# transition to JUMP when falling off an edge
	if (state in [State.IDLE, State.RUN]) and (!is_on_floor()):
		change_state(State.JUMP)
	if (state == State.CROUCH) and !down:
		change_state(State.IDLE)
	if state == State.CLIMB:
		if climb:
			velocity.y = -climb_speed
			if !$AnimationPlayer.is_playing():
				$AnimationPlayer.play("climb")
		elif down:
			velocity.y = climb_speed
			if !$AnimationPlayer.is_playing():
				$AnimationPlayer.play("climb")
		else:
			velocity.y = 0
			$AnimationPlayer.stop(true)
		if not is_on_ladder:
			change_state(State.IDLE)

func _physics_process(delta):
	if state != State.CLIMB:
		velocity.y += gravity * delta
	get_input()
	if new_anim != anim:
		anim = new_anim
		$AnimationPlayer.play(anim)
	# move the player
	velocity = move_and_slide(velocity, Vector2(0, -1))
	if state == State.HURT:
		return
	for idx in range(get_slide_count()):
		var collision = get_slide_collision(idx)
		if collision.collider.name == "Danger":
			hurt()
		if collision.collider.is_in_group("enemies"):
			var player_feet = (position + $CollisionShape2D.shape.extents).y
			if player_feet < collision.collider.position.y:
				collision.collider.take_damage()
				velocity.y = -200
			else:
				hurt()
	if state == State.JUMP:
		if velocity.y > 0:
			new_anim = "jump_down"
		if is_on_floor():
			change_state(State.IDLE)
			$Dust.emitting = true
	if position.y > 1000:
		change_state(State.DEAD)

func hurt():
	if state != State.HURT:
		$HurtSound.play()
		change_state(State.HURT)
